
---
  
	██████╗  █████╗ ███████╗██╗  ██╗    ██╗   ██╗████████╗██╗██╗     ███████╗
	██╔══██╗██╔══██╗██╔════╝██║  ██║    ██║   ██║╚══██╔══╝██║██║     ██╔════╝
	██████╔╝███████║███████╗███████║    ██║   ██║   ██║   ██║██║     ███████╗
	██╔══██╗██╔══██║╚════██║██╔══██║    ██║   ██║   ██║   ██║██║     ╚════██║
	██████╔╝██║  ██║███████║██║  ██║    ╚██████╔╝   ██║   ██║███████╗███████║
	╚═════╝ ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝     ╚═════╝    ╚═╝   ╚═╝╚══════╝╚══════╝

---

>**Bash_Utils**  
>*version: 0.1.0*  

---

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-red.svg)](http://www.gnu.org/licenses/gpl-3.0)

# description

some tools for scripts and bash shell  

# install

```bash
git clone https://gitlab.com/Do-IT.dev/bash_utils.git  
chmod -v u+x bash_utils/*.sh
PATH="${PATH}:${PWD}/bash_utils" #~ add this line in ~/.bashrc file to permanently access to bash_utils tools by the PATH variable
```

# tools

* color_code_finder.sh  
	list all color codes to use with the echo built-in command
	and display the result

```bash
color_code_finder.sh
```

![color_code_finder-screenshots.gif](screenshots/color_code_finder-screenshots.gif)

---

>*Copyright (C) 2020 Do I.T. <do-it.dev@protonmail.com>*  
