#!/usr/bin/env bash
#
#  Author: Do I.T. <do-it.dev@protonmail.com>
#  Date: 2020-07-03 13:53:04 +0200 (Fri, 3 Jul 2020)
#
#  https://gitlab.com/Do-IT.dev/bash_utils
#
#  Copyright (C) 2020 Do I.T.
#
#  This file is part of Bash_Utils.
#
#  Bash_Utils is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  Bash_Utils is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
#


_default_variable()
{
	ls_fg_code_unit='0;1;2;3;4;5;6;7'
	ls_fg_code_unit_key='e;r;t;y;u;i;o;p'
	default_fg_code_unit='7'

	ls_fg_code_dozen='3;9'
	ls_fg_code_dozen_key='c;v'
	default_fg_code_dozen='3'

	ls_bg_code_unit='0;1;2;3;4;5;6;7'
	ls_bg_code_unit_key='s;d;f;g;h;j;k;l'
	default_bg_code_unit='0'

	ls_bg_code_dozen='4;10'
	ls_bg_code_dozen_key='b;n'
	default_bg_code_dozen='4'

	ls_attr_code='0;1;2;3;4;5;6;7;8;9'
	ls_attr_code_key='0;1;2;3;4;5;6;7;8;9'
	default_attr_code='0'
}

# shellcheck disable=SC2120
_line_return()
{
	for ((n=1;n<="${1:-1}";n+=1)); do
		echo ''
	done
}

_set_color_code()
{
	fg_code="${fg_code_dozen:=${default_fg_code_dozen}}${fg_code_unit:=${default_fg_code_unit}}"
	bg_code="${bg_code_dozen:=${default_bg_code_dozen}}${bg_code_unit:=${default_bg_code_unit}}"
	color_code="\e[${attr_code:=${default_attr_code}};${fg_code};${bg_code}m"
}
_colorize(){ _set_color_code; echo -en "${color_code}";}
_decolorize(){ echo -en "\e[0m";}
_colorized_display(){ _colorize; "$@"; _decolorize;}

_array_builder()
{
	array_title="$1"
	value_set="$2"
	IFS=";" read -r -a column_entry <<< "$3"
	IFS=";" read -r -a column_index <<< "$4"
	IFS=";" read -r -a line_entry <<< "$5"
	IFS=";" read -r -a line_index <<< "$6"

	width='7'
	left_width=$((${line_entry[@]:+${width}} + 1))
	right_width=$((${line_entry[@]:+${width}} + 1))
	total_width=$((left_width + width * ${#column_entry[@]} + right_width))

	divider='========' #; divider="${divider}${divider}${divider}"
	if [[ -n ${line_entry[*]} ]]; then
		entry_cell_format="[  *%1s ]"
	else
		entry_cell_format="[   %1s ]"
	fi
	code_cell_format='[ %3s ]'
	index_cell_format=' ( %1.1s ) '

	_title_line(){ printf "%${total_width}s\n" "[ ${array_title^^} : \e[${value_set}m ]";}
	_wrapping_line()
	{
		# left
		_colorize
		printf "%-${left_width}.${left_width}s" "${divider}"
		# center
		for ((c=0;c<"${#column_entry[@]}";c+=1)); do
			printf "%${width}.${width}s" "${divider}"
		done
		# right
		printf "%${right_width}.${right_width}s" "${divider}"
		_decolorize
		_line_return
	}
	_border_line()
	{
		cell_format="$1"
		read -r -a raw_entry <<< "${@:2}"
		# left
		_colorized_display printf "%-${left_width}.${left_width}s" "${divider}"
		# center
		for ((r=0;r<"${#raw_entry[@]}";r+=1)); do
			# shellcheck disable=SC2059
			printf "${cell_format}" "${raw_entry[$r]}"
		done
		# right
		_colorized_display printf "%${right_width}.${right_width}s" "${divider}"
		_line_return
	}
	_top_line(){ _border_line "${entry_cell_format}" "${column_entry[@]}";}
	_bottom_line(){ _border_line "${index_cell_format}" "${column_index[@]}";}

	############################# array #############################
	_title_line
	_wrapping_line
	_top_line

	nb_line="${#line_entry[@]}"
	for ((l=0;l<"${nb_line/#0/1}";l+=1)); do
		# left
		if [[ -n ${line_entry[*]} ]]; then
			# shellcheck disable=SC2059
			printf "${code_cell_format} " "${line_entry[$l]}*"
		else
			printf "%-${left_width}.${left_width}s" ' '
		fi
		# center
		for ((c=0;c<"${#column_entry[@]}";c+=1)); do
			code_value="${line_entry[$l]}${column_entry[$c]}"
			echo -en "\e[${attr_code};${fg_code};${bg_code};${code_value}m"
			# shellcheck disable=SC2059
			printf "${code_cell_format}" "${code_value}"
			_decolorize
		done
		# right
		if [[ -n ${line_entry[*]} ]]; then
			# shellcheck disable=SC2059
			printf " ${index_cell_format}" "${line_index[$l]}"
		else
			printf "%${right_width}.${right_width}s" ' '
		fi
		_line_return
	done

	_bottom_line
	_wrapping_line
	_line_return
	############################# array #############################
}

_fg_color_code_array(){ _array_builder 'foreground color code' "${fg_code}" "${ls_fg_code_unit}" "${ls_fg_code_unit_key}" "${ls_fg_code_dozen}" "${ls_fg_code_dozen_key}";}
_bg_color_code_array(){ _array_builder 'background color code' "${bg_code}" "${ls_bg_code_unit}" "${ls_bg_code_unit_key}" "${ls_bg_code_dozen}" "${ls_bg_code_dozen_key}";}
_attr_color_code_array(){ _array_builder 'attribut code' "${attr_code}" "${ls_attr_code}" "${ls_attr_code_key}";}
_all_color_code_array()
{
	_attr_color_code_array
	_fg_color_code_array
	_bg_color_code_array
}

_color_code_result()
{
	echo -en "[!]   Let's see the result of your color choice ( "
	echo -n "\\e[${attr_code};${fg_code};${bg_code}m"
	echo -e " ) ..."
	_colorized_display echo -en "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sit cras."
	_line_return
}

_link_key_to_code()
{
	key="$1"
	IFS=";" read -r -a ls_key <<< "$2"
	IFS=";" read -r -a ls_code <<< "$3"
	for ((i=0;i<"${#ls_key[@]}";i+=1)); do
		if [[ "${key}" == "${ls_key[$i]}" ]]; then
			code="${ls_code[$i]}"
			break
		fi
	done
}

_asking_prompt()
{
	echo -en "[?]   Press any character between brackets to change color code\n\t or type 'q' to quit : "
	read -r -n 1 key
	_line_return
	if [[ "${key}" == [qQ] ]]; then
		return 1
	elif [[ "${key}" =~ ${ls_fg_code_unit_key//;/|}|${ls_fg_code_dozen_key//;/|} ]]; then
		if [[ "${key}" =~ ${ls_fg_code_unit_key//;/|} ]]; then
			_link_key_to_code "${key}" "${ls_fg_code_unit_key}" "${ls_fg_code_unit}"
			fg_code_unit="${code}"
		elif [[ "${key}" =~ ${ls_fg_code_dozen_key//;/|} ]]; then
			_link_key_to_code "${key}" "${ls_fg_code_dozen_key}" "${ls_fg_code_dozen}"
			fg_code_dozen="${code}"
		fi
	elif [[ "${key}" =~ ${ls_bg_code_unit_key//;/|}|${ls_bg_code_dozen_key//;/|} ]]; then
		if [[ "${key}" =~ ${ls_bg_code_unit_key//;/|} ]]; then
			_link_key_to_code "${key}" "${ls_bg_code_unit_key}" "${ls_bg_code_unit}"
			bg_code_unit="${code}"
		elif [[ "${key}" =~ ${ls_bg_code_dozen_key//;/|} ]]; then
			_link_key_to_code "${key}" "${ls_bg_code_dozen_key}" "${ls_bg_code_dozen}"
			bg_code_dozen="${code}"
		fi
	elif [[ "${key}" =~ ${ls_attr_code_key//;/|} ]]; then
		_link_key_to_code "${key}" "${ls_attr_code_key}" "${ls_attr_code}"
		attr_code="${code}"
	fi
}

_main_display()
{
	while true; do
		clear
		_line_return
		_set_color_code
		_all_color_code_array
		_color_code_result
		if ! _asking_prompt; then
			break
		fi
	done
}

_default_variable
_main_display
exit 0
